﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI_bazy_danych
{
    public partial class NewRecord : Form
    {
        private string add_to_table;
        private List<KeyValuePair<string, string>> col_val;

        public int _NewRecord()
        {
            InitializeComponent();
            return 0;
        }

        public NewRecord(string table)
        {
            InitializeComponent();

            col_val = new List<KeyValuePair<string, string>>();

            add_to_table = table;
            this.label1.Text += table;

            picken_column.DropDownStyle = ComboBoxStyle.DropDownList;
            picken_column.Items.Clear();

            picken_column.SelectedValueChanged +=
                new EventHandler(SaveValue);

            

            foreach (var column in DataBaseFramework.Instance.GetTableColumns(table))
            {
                picken_column.Items.Add(column);
            }

            //picken_column.SelectedIndex = 0;
        }

        private void SaveValue(object sender, EventArgs e)
        {
            //if (valForCoulmn.TextLength <= 0)
            //    return;

            //col_val.Add(new KeyValuePair<string, string>(picken_column.Text, valForCoulmn.Text));
            valForCoulmn.Text = "";
            valForCoulmn.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //col_val.Add(new KeyValuePair<string, string>(picken_column.Text, valForCoulmn.Text));
            valForCoulmn.Text = "";

            string message = "";

            foreach(var x in col_val)
            {
                message += "\n  " + x.Key + "  " + x.Value;
            }

            MessageBox.Show(message);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (valForCoulmn.TextLength <= 0)
                return;

            if (picken_column.Text.Length <= 0)
                return;

            if (col_val.Exists(x => x.Key == picken_column.Text))
                return;

            col_val.Add(new KeyValuePair<string, string>(picken_column.Text, valForCoulmn.Text));
            lbRecordsToAdd.Items.Add(picken_column.Text + " : " + valForCoulmn.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var selectedItem = lbRecordsToAdd.Text;
            var itemToRemove = selectedItem.Split(' ');

            col_val.Remove(col_val.Find(x => x.Key == itemToRemove[0]));
            lbRecordsToAdd.Items.Remove(selectedItem);
        }
    }
}
