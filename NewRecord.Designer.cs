﻿namespace GUI_bazy_danych
{
    partial class NewRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.picken_column = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.valForCoulmn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.Sdsad = new System.Windows.Forms.Label();
            this.lbRecordsToAdd = new System.Windows.Forms.ListBox();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 143);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(540, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "Add Record";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Adding record for table: ";
            // 
            // picken_column
            // 
            this.picken_column.FormattingEnabled = true;
            this.picken_column.Location = new System.Drawing.Point(13, 56);
            this.picken_column.Name = "picken_column";
            this.picken_column.Size = new System.Drawing.Size(162, 21);
            this.picken_column.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Select Coulmn";
            // 
            // valForCoulmn
            // 
            this.valForCoulmn.Location = new System.Drawing.Point(225, 56);
            this.valForCoulmn.Name = "valForCoulmn";
            this.valForCoulmn.Size = new System.Drawing.Size(226, 20);
            this.valForCoulmn.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Set Value for Column";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(476, 53);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Save Value";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Sdsad
            // 
            this.Sdsad.AutoSize = true;
            this.Sdsad.Location = new System.Drawing.Point(569, 9);
            this.Sdsad.Name = "Sdsad";
            this.Sdsad.Size = new System.Drawing.Size(73, 13);
            this.Sdsad.TabIndex = 7;
            this.Sdsad.Text = "Values to Add";
            // 
            // lbRecordsToAdd
            // 
            this.lbRecordsToAdd.FormattingEnabled = true;
            this.lbRecordsToAdd.Location = new System.Drawing.Point(572, 25);
            this.lbRecordsToAdd.Name = "lbRecordsToAdd";
            this.lbRecordsToAdd.Size = new System.Drawing.Size(198, 121);
            this.lbRecordsToAdd.TabIndex = 8;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(572, 153);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(198, 28);
            this.button3.TabIndex = 9;
            this.button3.Text = "Remove Selected";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // NewRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 193);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.lbRecordsToAdd);
            this.Controls.Add(this.Sdsad);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.valForCoulmn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.picken_column);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Record to Table";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox picken_column;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox valForCoulmn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label Sdsad;
        private System.Windows.Forms.ListBox lbRecordsToAdd;
        private System.Windows.Forms.Button button3;
    }
}