﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace GUI_bazy_danych
{
    //public static class DataBaseFramework
    //{
    //    SqlConnection connection;

    //    public DataBaseFramework()
    //    {
    //        try
    //        {
    //            string cs = @"Data Source=DESKTOP-QP0K0LI\SQLEXPRESS;Initial Catalog=hotel;Integrated Security=True";
    //            connection = new SqlConnection(cs);
    //            connection.Open();
    //        }
    //        catch (Exception ex)
    //        {
                
    //        }
            
    //    }

    //    public DataTable ReadToDataTable(string q)
    //    {
    //        var table = new DataTable();

    //        using (var command = new SqlCommand { Connection = connection })
    //        {

    //            if (connection.State == ConnectionState.Open)
    //            {
    //                connection.Close();
    //            }

    //            connection.Open();

    //            try
    //            {
    //                command.CommandText = q;
    //                table.Load(command.ExecuteReader());

    //                return table;

    //                //bindingSource1.DataSource = table;

    //                //dataGridView1.ReadOnly = true;
    //                //dataGridView1.DataSource = bindingSource1;

    //            }
    //            catch (SqlException ex)
    //            {


    //            }

    //                return null;
    //        }

    //    }

    //    public List<string> GetTables()
    //    {
    //        if (connection.State == ConnectionState.Open)
    //        {
    //            connection.Close();
    //        }

    //        connection.Open();
    //        DataTable schema = connection.GetSchema("Tables");
    //            List<string> TableNames = new List<string>();
    //            foreach (DataRow row in schema.Rows)
    //            {
    //                TableNames.Add(row[2].ToString());
    //            }
    //            return TableNames;
    //    }
    //}

    public sealed class DataBaseFramework
    {
        private static SqlConnection connection;
        private static readonly DataBaseFramework instance = new DataBaseFramework();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static DataBaseFramework()
        {

        }

        private DataBaseFramework()
        {
            try
            {
                string cs = @"Data Source=DESKTOP-QP0K0LI\SQLEXPRESS;Initial Catalog=hotel;Integrated Security=True";
                connection = new SqlConnection(cs);
                //connection.Open();
            }
            catch (Exception ex)
            {

            }
        }

        public static DataBaseFramework Instance
        {
            get
            {
                return instance;
            }
        }

        public DataTable ReadToDataTable(string q)
        {
            var table = new DataTable();

            using (var command = new SqlCommand { Connection = connection })
            {

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                connection.Open();

                try
                {
                    command.CommandText = q;
                    table.Load(command.ExecuteReader());

                    connection.Close();

                    return table;
                    //bindingSource1.DataSource = table;

                    //dataGridView1.ReadOnly = true;
                    //dataGridView1.DataSource = bindingSource1;

                }
                catch (SqlException ex)
                {


                }

                connection.Close();

                return null;
            }

        }

        public List<string> GetTables()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            connection.Open();
            DataTable schema = connection.GetSchema("Tables");
            List<string> TableNames = new List<string>();
            foreach (DataRow row in schema.Rows)
            {
                TableNames.Add(row[2].ToString());
            }

            connection.Close();

            return TableNames;
        }

        public List<string> GetTableColumns(string tablename)
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            connection.Open();

            DataTable table = connection.GetSchema("Columns", new string[] { null, null, tablename });


            connection.Close();

            return (from r in table.AsEnumerable()
                    select r.Field<string>("COLUMN_NAME")).ToList();
        }

        public bool AddRecordForTable(string tablename, List<KeyValuePair<string, string>> values)
        {
            var columns = this.GetTableColumns(tablename);

            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            connection.Open();

            var command = new SqlCommand { Connection = connection };

//            INSERT INTO table_name
//VALUES(value1, value2, value3, ...);

            string query = "INSERT INTO" + tablename + "VALUES(";

            var it = 0;
            var last_indx = columns.Count - 1;

            foreach(var column in columns)
            {
                if(values.Exists(x => x.Key == column))
                {
                    query += values.Find(x => x.Key == column).Value;

                    if (it != last_indx)
                        query += ", ";
                }
                else
                {
                    query += "0";

                    if (it != last_indx)
                        query += ", ";
                }

                it++;
            }

            query += ");";

            command.CommandText = query;

            command.BeginExecuteReader();

            connection.Close();


            return true;
        }


        //public bool AuthenticateUser(string login, string pass)
        //{

        //}
    }
}
