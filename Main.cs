﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace GUI_bazy_danych
{
    public partial class Main : Form
    {
        int tick = 0;

        public int _Main()
        {
            InitializeComponent();
            picken_table.DropDownStyle = ComboBoxStyle.DropDownList;
            timer1.Start();
            cbReadOnly.Checked = true;
            cbAnim.Checked = true;

            picken_table.Items.Clear();

            foreach(var table in DataBaseFramework.Instance.GetTables())
            {
                picken_table.Items.Add(table);
            }

            picken_table.SelectedValueChanged +=
                new EventHandler(reloadTable);

            return 0;
        }

        private void reloadTable(object sender, EventArgs e)
        {
            if(picken_table.Text.Length > 0)
            {
                var table = DataBaseFramework.Instance.ReadToDataTable("SELECT * from " + picken_table.Text);
                bindingSource1.DataSource = table;

                dataGridView1.ReadOnly = cbReadOnly.Checked;
                dataGridView1.DataSource = bindingSource1;
            }
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            NewRecord nr = new NewRecord(picken_table.Text);
            nr.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (cbAnim.Checked)
            {
                if (!label3.Enabled)
                    label3.Enabled = true;

                string[] animation = { "___HOTEL", "__HOTEL_", "_HOTEL__", "HOTEL___", "_HOTEL__", "__HOTEL_" };


                label3.Text = animation[tick];

                //if (label3.Text.Length > 10)
                //    label3.Text = "Loading";

                tick++;

                if (tick >= animation.Length)
                    tick = 0;
            }
            else if(label3.Enabled)
            {
                label3.Text = "      HOTEL";
                label3.Enabled = false;
            }

           


            //checkbox updates
            dataGridView1.ReadOnly = cbReadOnly.Checked;

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //var Q = from p in en.Pokoje select p;
            //dataGridView1.DataSource = Q.ToList();
        }

        private void cbReadOnly_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
